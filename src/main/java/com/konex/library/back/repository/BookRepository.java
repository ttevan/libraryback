package com.konex.library.back.repository;

import com.konex.library.back.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    public List<Book> findByName(String name);
}

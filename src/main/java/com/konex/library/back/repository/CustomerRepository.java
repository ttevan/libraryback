package com.konex.library.back.repository;

import com.konex.library.back.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    public Customer findByUsername(String username);
    public Customer findByUsernameAndPassword(String username, String password);
}


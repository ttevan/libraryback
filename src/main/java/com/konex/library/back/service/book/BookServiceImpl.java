package com.konex.library.back.service.book;

import com.konex.library.back.entity.Book;
import com.konex.library.back.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public List<Book> listAllBook() {
        return bookRepository.findAll();
    }

    @Override
    public Book getBook(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public Book createBook(Book book) {
        book.setCreateAt(new Date());
        book.setStatus("created");
        return bookRepository.save(book);
    }

    @Override
    public Book updateBook(Book book) {
        Book bookDb = getBook(book.getId());
        if (bookDb == null) {
            return null;
        }
        return bookRepository.save(book);
    }

    @Override
    public Book deleteBook(Long id) {
        Book bookDb = getBook(id);
        if (bookDb == null) {
            return null;
        }
        bookDb.setStatus("deleted");
        return bookRepository.save(bookDb);
    }

    @Override
    public Book updateStock(Long id, Double quantity) {
        Book bookDb = getBook(id);
        if (bookDb == null) {
            return null;
        }
        bookDb.setStock(quantity);
        return bookRepository.save(bookDb);
    }
}
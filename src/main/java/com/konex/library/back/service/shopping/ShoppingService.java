package com.konex.library.back.service.shopping;

import com.konex.library.back.entity.Shopping;

import java.util.List;

public interface ShoppingService {
    public List<Shopping> listAll();
    public Shopping get(Long id);
    public List<Shopping> getByIdCustomerAndStatus(Long idCustomer, String status);
    public List<Shopping> getByIdCustomer(Long idCustomer);
    public Shopping create(Shopping shopping);
    public Shopping updateQuantity(Long id, int quantity);
    public Shopping update(Shopping shopping);
    public Shopping delete(Long id);
}
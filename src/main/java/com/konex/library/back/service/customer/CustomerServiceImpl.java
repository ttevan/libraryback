package com.konex.library.back.service.customer;

import com.konex.library.back.entity.Customer;
import com.konex.library.back.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public List<Customer> listAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer get(Long id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public Customer loginByUsername(String username) {
        return customerRepository.findByUsername(username);
    }

    @Override
    public Customer login(String user, String pass) {
        Customer customerDB = customerRepository.findByUsernameAndPassword(user, pass);
        return customerDB;
    }

    @Override
    public Customer create(Customer customer) {
        Customer customerDB = customerRepository.findByUsername(customer.getUsername());
        if (customerDB != null) {
            return customerDB;
        }
        customer.setStatus("created");
        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Customer customer) {
        Customer customerDB = get(customer.getId());
        if (customerDB == null) {
            return null;
        }
        return customerRepository.save(customer);
    }

    @Override
    public Customer delete(Long id) {
        Customer customerDB = get(id);
        if (customerDB == null) {
            return null;
        }
        customerDB.setStatus("deleted");
        return customerRepository.save(customerDB);
    }
}

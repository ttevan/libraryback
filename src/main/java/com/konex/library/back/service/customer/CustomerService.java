package com.konex.library.back.service.customer;

import com.konex.library.back.entity.Customer;

import java.util.List;

public interface CustomerService {
    public List<Customer> listAll();
    public Customer get(Long id);
    public Customer loginByUsername(String username);
    public Customer login(String user, String pass);
    public Customer create(Customer customer);
    public Customer update(Customer customer);
    public Customer delete(Long id);
}

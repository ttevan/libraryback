package com.konex.library.back.service.book;

import com.konex.library.back.entity.Book;

import java.util.List;

public interface BookService {

    public List<Book> listAllBook();
    public Book getBook(Long id);

    public Book createBook(Book book);
    public Book updateBook(Book book);
    public Book deleteBook(Long id);
    public Book updateStock(Long id, Double quantity);
}

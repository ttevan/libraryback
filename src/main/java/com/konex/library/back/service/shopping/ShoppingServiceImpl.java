package com.konex.library.back.service.shopping;

import com.konex.library.back.entity.Shopping;
import com.konex.library.back.repository.ShoppingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ShoppingServiceImpl implements ShoppingService {

    @Autowired
    ShoppingRepository shoppingRepository;

    @Override
    public List<Shopping> listAll() {
        return shoppingRepository.findAll();
    }

    @Override
    public Shopping get(Long id) {
        return shoppingRepository.findById(id).orElse(null);
    }

    @Override
    public List<Shopping> getByIdCustomerAndStatus(Long idCustomer, String status) {
        return shoppingRepository.findByIdCustomerAndStatus(idCustomer, status);
    }

    @Override
    public List<Shopping> getByIdCustomer(Long idCustomer) {
        return shoppingRepository.findByIdCustomer(idCustomer);
    }

    @Override
    public Shopping create(Shopping shopping) {
        List<Shopping> shoppingDB = shoppingRepository.findByIdCustomerAndStatus(shopping.getIdCustomer(), Shopping.ENCARRITO);
        if (!shoppingDB.isEmpty()) {
            for (Shopping item : shoppingDB) {
                if (item.getIdBook() == shopping.getIdBook()) {
                    return shopping;
                }
            }
        }
        shopping.setStatus(Shopping.ENCARRITO);
        shopping.setCreateAt(new Date());
        return shoppingRepository.save(shopping);
    }

    @Override
    public Shopping updateQuantity(Long id, int quantity) {
        Shopping shoppingDB = get(id);
        if (shoppingDB == null) {
            return null;
        }
        shoppingDB.setQuantity(quantity);
        return update(shoppingDB);
    }

    @Override
    public Shopping update(Shopping shopping) {
        Shopping shoppingDB = get(shopping.getId());
        if (shoppingDB == null) {
            return null;
        }
        return shoppingRepository.save(shopping);
    }

    @Override
    public Shopping delete(Long id) {
        Shopping shoppingDB = get(id);
        if (shoppingDB == null) {
            return null;
        }
        shoppingDB.setStatus(Shopping.CANCELADO);
        return shoppingRepository.save(shoppingDB);
    }
}
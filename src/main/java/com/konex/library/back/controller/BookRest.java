package com.konex.library.back.controller;

import com.konex.library.back.entity.Book;
import com.konex.library.back.service.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping(value="/books")
@RestController
public class BookRest {

    @Autowired
    private BookService bookService;

    @GetMapping
    public ResponseEntity<List<Book>> listBook() {
        List<Book> books = bookService.listAllBook();
        if (books.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(books);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Book> getBook(@PathVariable("id") Long id) {
        Book book = bookService.getBook(id);
        if (book == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(book);
    }

    @GetMapping(value="/{id}/{quantity}/stock")
    public ResponseEntity<Book> updateStock(@PathVariable Long id, @PathVariable Double quantity) {
        Book book = bookService.updateStock(id, quantity);
        if (book == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(book);
    }
}

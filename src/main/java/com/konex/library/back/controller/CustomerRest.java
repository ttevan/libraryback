package com.konex.library.back.controller;

import com.konex.library.back.entity.AuthRequest;
import com.konex.library.back.entity.Customer;
import com.konex.library.back.security.JwtUtil;
import com.konex.library.back.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping(value="/customers")
@RestController
public class CustomerRest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/generateToken")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            System.out.println("Trye");
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        } catch (Exception ex) {
            return null;
        }
        return jwtUtil.generateToken(authRequest.getUsername());
    }

    @PostMapping("/login")
    public ResponseEntity<Customer> get(@RequestBody AuthRequest authRequest) {
        System.out.println(authRequest.getUsername()+" - "+authRequest.getPassword());
        Customer customer = customerService.login(authRequest.getUsername(),authRequest.getPassword());
        if (customer == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(customer);
    }
}

package com.konex.library.back.controller;

import com.konex.library.back.entity.Book;
import com.konex.library.back.entity.Shopping;
import com.konex.library.back.service.book.BookService;
import com.konex.library.back.service.customer.CustomerService;
import com.konex.library.back.service.shopping.ShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping(value="/shoppings")
@RestController
public class ShoppingRest {

    @Autowired
    ShoppingService shoppingService;

    @Autowired
    BookService bookService;

    @GetMapping("/quantity/{idCustomer}")
    public ResponseEntity<Integer> getQuantityByCustomer(@PathVariable Long idCustomer) {
        List<Shopping> shoppings = shoppingService.getByIdCustomerAndStatus(idCustomer, Shopping.ENCARRITO);
        return ResponseEntity.ok(shoppings.size());
    }

    @PostMapping
    public ResponseEntity<Shopping> create(@RequestBody Shopping shopping) {
        Shopping shoppingDB = shoppingService.create(shopping);
        return ResponseEntity.status(HttpStatus.CREATED).body(shoppingDB);
    }

    @GetMapping("/byCustomer/{idCustomer}")
    public ResponseEntity<List<Shopping>> getByCustomer(@PathVariable Long idCustomer) {
        List<Shopping> shoppings = shoppingService.getByIdCustomerAndStatus(idCustomer, Shopping.ENCARRITO);
        shoppings.forEach(element -> {
            Book book = bookService.getBook(element.getIdBook());
            if (book != null)
                element.setBook(book);
        });
        return ResponseEntity.ok(shoppings);
    }

    @PutMapping
    public ResponseEntity<Shopping> update(@RequestBody Shopping shopping) {
        Shopping shoppingDB = shoppingService.update(shopping);
        if (shoppingDB == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shoppingDB);
    }

    @PostMapping("/buy")
    public ResponseEntity<String> buy(@RequestBody List<Shopping> shoppings) {
        shoppings.forEach(element -> {
            Book book = bookService.getBook(element.getIdBook());
            if (book != null) {
                Double quantity = (book.getStock() - element.getQuantity());
                book.setStock(quantity);
                bookService.updateBook(book);
            }
            element.setStatus(Shopping.FINALIZADO);
            update(element);
        });
        return ResponseEntity.ok("Buy finished");
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Shopping> delete(@PathVariable Long id) {
        Shopping shopping = shoppingService.delete(id);
        if (shopping == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shopping);
    }

}
